#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include "math.h"
#include "./cuda.h"

#define CHECK(func) \
{   \
    int ret; \
    if ((ret = func) != CUDA_SUCCESS) { \
        printf("Function Call %s retval %d Failed!\n", #func, ret);    \
        exit(1);    \
    }   \
}

/********************************/
float __cal_time(struct timeval *start, struct timeval *end)
{
    return (end->tv_sec * 1000000 + end->tv_usec) -
            (start->tv_sec * 1000000 + start->tv_usec);
}

/********************************/
CUdevice device;
CUcontext context;
CUmodule module;
CUfunction empty, mem_access, mem_accessExPre, mem_accessExPost;
CUfunction mem_accessPrt, ImmPrt, mem_accessWait, mem_accessGet;
void initCUDA(void)
{
    int devCount = 0;
    int value;

    CHECK(cuInit(0));
    CHECK(cuDeviceGetCount(&devCount));

    if (devCount == 0) {
        printf("No Device!\n");
        exit(1);
    }

    CHECK(cuDeviceGet(&device, 0));
    CHECK(cuCtxCreate(&context, 0, device));

    CHECK(cuDeviceGetAttribute(&value, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device));
    printf("attr compute cap major: %d\n", value);
    CHECK(cuDeviceGetAttribute(&value, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device));
    printf("attr compute cap minor: %d\n", value);

    CHECK(cuModuleLoad(&module, "test.cubin"));
    CHECK(cuModuleGetFunction(&empty, module, "empty"));
    CHECK(cuModuleGetFunction(&mem_access, module, "mem_access"));
    CHECK(cuModuleGetFunction(&mem_accessExPre, module, "mem_accessExPre"));
    CHECK(cuModuleGetFunction(&mem_accessExPost, module, "mem_accessExPost"));
    CHECK(cuModuleGetFunction(&mem_accessExPost, module, "mem_accessExPost"));
    CHECK(cuModuleGetFunction(&mem_accessPrt, module, "mem_accessPrt"));
    CHECK(cuModuleGetFunction(&ImmPrt, module, "ImmPrt"));
    CHECK(cuModuleGetFunction(&mem_accessWait, module, "mem_accessWait"));
    CHECK(cuModuleGetFunction(&mem_accessGet, module, "mem_accessGet"));
}

void exitCUDA(void)
{
    CHECK(cuModuleUnload(module));
    CHECK(cuCtxDestroy(context));
}
/********************************/
#define TEST_LOOP (50000)
#define TEST_TIME (10)

//performance
static void test_function_basic_tp(void)
{
    CUstream stream;
    struct timeval start, end;
    float sw_time;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));

    gettimeofday(&start, NULL);
    for (int i = 0; i < TEST_LOOP; i++) {
        CHECK(cuLaunchKernel(empty, 1, 1, 1, 1, 1, 1, 0, stream, NULL, NULL));
    }
    CHECK(cuStreamSynchronize(stream));
    gettimeofday(&end, NULL);

    CHECK(cuStreamDestroy(stream));

    sw_time = __cal_time(&start, &end);
    sw_time /= TEST_LOOP;
    printf("basic cuLaunchKernel throughput is %6.2f us\n", sw_time);

}

static void test_function_basic_lat(void)
{
    CUstream stream;
    struct timeval start, end;
    float sw_time = 0;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));

    for (int i = 0; i < TEST_LOOP; i++) {
        gettimeofday(&start, NULL);
        CHECK(cuLaunchKernel(empty, 1, 1, 1, 1, 1, 1, 0, stream, NULL, NULL));
        CHECK(cuStreamSynchronize(stream));
        gettimeofday(&end, NULL);
        sw_time += __cal_time(&start, &end);
    }

    CHECK(cuStreamDestroy(stream));

    sw_time /= TEST_LOOP;
    printf("basic cuLaunchKernel latency is %6.2f us\n", sw_time);

}

static void test_ex_function_basic_tp(void)
{
    CUstream stream;
    CUlaunchConfig cfg = {0};
    struct timeval start, end;
    float sw_time;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));

    /* config basic */
    cfg.gridDimX = 1;
    cfg.gridDimY = 1;
    cfg.gridDimZ = 1;
    cfg.blockDimX = 1;
    cfg.blockDimY = 1;
    cfg.blockDimZ = 1;
    cfg.hStream = stream;

    gettimeofday(&start, NULL);
    for (int i = 0; i < TEST_LOOP; i++) {
        CHECK(cuLaunchKernelEx(&cfg, empty, NULL, NULL));
    }
    CHECK(cuStreamSynchronize(stream));
    gettimeofday(&end, NULL);

    CHECK(cuStreamDestroy(stream));

    sw_time = __cal_time(&start, &end);
    sw_time /= TEST_LOOP;
    printf("basic cuLaunchKernelEx throughput is %6.2f us\n", sw_time);
}

static void test_ex_function_basic_lat(void)
{
    CUstream stream;
    CUlaunchConfig cfg = {0};
    struct timeval start, end;
    float sw_time = 0;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));

    /* config basic */
    cfg.gridDimX = 1;
    cfg.gridDimY = 1;
    cfg.gridDimZ = 1;
    cfg.blockDimX = 1;
    cfg.blockDimY = 1;
    cfg.blockDimZ = 1;
    cfg.hStream = stream;

    for (int i = 0; i < TEST_LOOP; i++) {
        gettimeofday(&start, NULL);
        CHECK(cuLaunchKernelEx(&cfg, empty, NULL, NULL));
        CHECK(cuStreamSynchronize(stream));
        gettimeofday(&end, NULL);
        sw_time += __cal_time(&start, &end);
    }

    CHECK(cuStreamDestroy(stream));

    sw_time /= TEST_LOOP;
    printf("basic cuLaunchKernelEx latency is %6.2f us\n", sw_time);
}

#define KERNEL_LOOP (5000)
static void test_synchronize_time(void)
{
    CUstream stream;
    CUdeviceptr d_a;
    void *params[] = {&d_a};
    CUlaunchConfig cfg = {0};
    CUlaunchConfig cfgEx = {0};
    CUlaunchAttribute attr[2];
    struct timeval start, end;
    float sw_time;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));
    CHECK(cuMemAlloc(&d_a, KERNEL_LOOP * sizeof(int)));

    /* config basic */
    cfg.gridDimX = 1;
    cfg.gridDimY = 1;
    cfg.gridDimZ = 1;
    cfg.blockDimX = 1;
    cfg.blockDimY = 1;
    cfg.blockDimZ = 1;
    cfg.hStream = stream;

    printf("test basic two kernel pair throughput...\n");
    cfg.numAttrs = 0;

    gettimeofday(&start, NULL);
    for (int i = 0; i < TEST_LOOP; i++) {
        CHECK(cuLaunchKernelEx(&cfg, mem_access, params, NULL));
        CHECK(cuLaunchKernelEx(&cfg, mem_access, params, NULL));
    }
    CHECK(cuStreamSynchronize(stream));
    gettimeofday(&end, NULL);
    sw_time = __cal_time(&start, &end);
    sw_time /= TEST_LOOP;
    printf("basic two kernel pair throughput is %6.2f us\n", sw_time);
    CHECK(cuStreamDestroy(stream));

    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));
    printf("test two dependence kernel pair throughput...\n");
    /* config attr */
    cfgEx.gridDimX = 1;
    cfgEx.gridDimY = 1;
    cfgEx.gridDimZ = 1;
    cfgEx.blockDimX = 1;
    cfgEx.blockDimY = 1;
    cfgEx.blockDimZ = 1;
    cfgEx.hStream = stream;
    attr[0].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION;
    attr[0].value.programmaticStreamSerializationAllowed = 1;
    cfgEx.attrs = attr;
    cfgEx.numAttrs = 1;

    gettimeofday(&start, NULL);
    for (int i = 0; i < TEST_LOOP; i++) {
        CHECK(cuLaunchKernelEx(&cfg, mem_accessExPre, params, NULL));
        CHECK(cuLaunchKernelEx(&cfgEx, mem_accessExPost, params, NULL));
    }
    CHECK(cuStreamSynchronize(stream));
    gettimeofday(&end, NULL);
    sw_time = __cal_time(&start, &end);
    sw_time /= TEST_LOOP;
    printf("two dependence kernel pair throughput is %6.2f us\n", sw_time);

    CHECK(cuStreamDestroy(stream));
    CHECK(cuMemFree(d_a))
}

//function
static void test_max_embed_number(void)
{
    CUstream stream;
    CUdeviceptr d_a, d_b, d_c;
    void *params[] = {&d_a, &d_b, &d_c};
    CUlaunchConfig cfg = {0};
    CUlaunchConfig cfgEx = {0};
    CUlaunchAttribute attr[2];
    int test = 0;

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));
    CHECK(cuMemAlloc(&d_a, KERNEL_LOOP * sizeof(int)));
    CHECK(cuMemAlloc(&d_b, sizeof(int)));
    CHECK(cuMemAlloc(&d_c, sizeof(int)));

    CHECK(cuMemcpyHtoD(d_b, &test, sizeof(int)));
    CHECK(cuMemcpyHtoD(d_c, &test, sizeof(int)));

    /* config basic */
    cfg.gridDimX = 228;
    cfg.gridDimY = 1;
    cfg.gridDimZ = 1;
    cfg.blockDimX = 1024;
    cfg.blockDimY = 1;
    cfg.blockDimZ = 1;
    cfg.sharedMemBytes = 49152;
    cfg.hStream = stream;
    cfg.numAttrs = 0;

    /* config attr */
    cfgEx.gridDimX = 1;
    cfgEx.gridDimY = 1;
    cfgEx.gridDimZ = 1;
    cfgEx.blockDimX = 1;
    cfgEx.blockDimY = 1;
    cfgEx.blockDimZ = 1;
    cfgEx.hStream = stream;
    attr[0].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION;
    attr[0].value.programmaticStreamSerializationAllowed = 1;
    cfgEx.attrs = attr;
    cfgEx.numAttrs = 1;

    printf("Launch one long time PRT, and serval programmatic ImmPrt\n");
    CHECK(cuLaunchKernelEx(&cfg, mem_accessPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, ImmPrt, params, NULL));

//    while (1) {
//        CHECK(cuMemcpyDtoH(&test, d_b, sizeof(int)));
//        printf("host d_b = %d\n", test);
//	sleep(1);
//    }

    CHECK(cuStreamSynchronize(stream));
    CHECK(cuMemcpyDtoH(&test, d_b, sizeof(int)));
    printf("d_b = %d\n", test);
    CHECK(cuMemcpyDtoH(&test, d_c, sizeof(int)));
    printf("d_c = %d\n", test);

    CHECK(cuStreamDestroy(stream));
    CHECK(cuMemFree(d_a))
    CHECK(cuMemFree(d_b))
    CHECK(cuMemFree(d_c))
}

//function
static void test_resource_util(void)
{
    CUstream stream;
    CUdeviceptr d_a;
    void *params[] = {&d_a};
    CUlaunchConfig cfg = {0};
    CUlaunchConfig cfgEx = {0};
    CUlaunchAttribute attr[2];

    CHECK(cuCtxSetCurrent(context));
    CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));
    CHECK(cuMemAlloc(&d_a, KERNEL_LOOP * sizeof(int)));

    /* config basic */
    cfg.gridDimX = 1;
    cfg.gridDimY = 1;
    cfg.gridDimZ = 1;
    cfg.blockDimX = 1;
    cfg.blockDimY = 1;
    cfg.blockDimZ = 1;
    cfg.hStream = stream;
    cfg.numAttrs = 0;

    /* config attr */
    cfgEx.gridDimX = 228;
    cfgEx.gridDimY = 1;
    cfgEx.gridDimZ = 1;
    cfgEx.blockDimX = 1024;
    cfgEx.blockDimY = 1;
    cfgEx.blockDimZ = 1;
    cfgEx.sharedMemBytes = 49152;
    cfgEx.hStream = stream;
    attr[0].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION;
    attr[0].value.programmaticStreamSerializationAllowed = 1;
    cfgEx.attrs = attr;
    cfgEx.numAttrs = 1;

    printf("Launch one small size long time PRT, and one big size programmatic mem_access\n");
    CHECK(cuLaunchKernelEx(&cfg, mem_accessPrt, params, NULL));
    CHECK(cuLaunchKernelEx(&cfgEx, mem_access, params, NULL));
    getchar();
    printf("Launch over\n");
    CHECK(cuLaunchKernelEx(&cfg, ImmPrt, params, NULL));

    CHECK(cuStreamSynchronize(stream));

    CHECK(cuStreamDestroy(stream));
    CHECK(cuMemFree(d_a))
}

int main(int argc, char **argv)
{
    printf("- Init...\n");
    initCUDA();

    printf("1.throughput...\n");
    for (int i = 0; i < TEST_TIME; i++) {
        test_function_basic_tp();
    }
    for (int i = 0; i < TEST_TIME; i++) {
        test_ex_function_basic_tp();
    }

    printf("2.latency...\n");
    for (int i = 0; i < TEST_TIME; i++) {
        test_function_basic_lat();
    }
    for (int i = 0; i < TEST_TIME; i++) {
        test_ex_function_basic_lat();
    }

    printf("3.programmatic sync time...\n");
    //for (int i = 0; i < TEST_TIME; i++) {
    for (int i = 0; i < 2; i++) {
        test_synchronize_time();
    }

    printf("4.programmatic dep embed number...\n");
    test_max_embed_number();

goto exit;
    printf("5.programmatic dep resource occupy...\n");
    test_resource_util();

exit:
    exitCUDA();
    return 0;
}

