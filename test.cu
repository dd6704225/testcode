#include <stdio.h>
#include <stdint.h>

extern "C" __global__ void empty(void)
{
}

#define KERNEL_LOOP (5000)
extern "C" __global__ void mem_access(int *a)
{
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
}

extern "C" __global__ void mem_accessExPre(int *a)
{
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
    cudaTriggerProgrammaticLaunchCompletion();
}

extern "C" __global__ void mem_accessExPost(int *a)
{
    cudaGridDependencySynchronize();
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
}

extern "C" __global__ void mem_accessPrt(int *a, int *b, int *c)
{
    cudaTriggerProgrammaticLaunchCompletion();
    for (int j = 0; j < 1000; j++) {
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
    }
    c[0] = b[0];
}

extern "C" __global__ void ImmPrt(int *a, int *b, int *c)
{
//    cudaGridDependencySynchronize();
    atomicAdd_system(&b[0], 1);
}

extern "C" __global__ void mem_accessWait(int *a, int *b, int *c)
{
    cudaTriggerProgrammaticLaunchCompletion();
    for (int j = 0; j < 1000; j++) {
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
    }
}

extern "C" __global__ void mem_accessGet(int *a, int *b, int *c)
{
    for (int j = 0; j < 1000; j++) {
    for (int i = 0; i < KERNEL_LOOP; i++) {
        a[i] = i;
    }
    }
    c[0] = b[0];
}

#if 1
#define KERN_PRT(string, arg...) \
	printf("[KERN][%s][%d]: [" string "]\n", __func__, __LINE__, ##arg);
#else
#define KERN_PRT(string, arg...) {}
#endif
extern "C" __global__ void event_trgAtMid(int time)
{
	volatile int i = time;
	volatile int j;
	int c;

	KERN_PRT("start loop %d", i);

	for (j = 0; j < i; j++) {
		c = j;
	}
	KERN_PRT("trigger prog launch");
    	cudaTriggerProgrammaticLaunchCompletion();

	for (j = 0; j < i; j++) {
		c = j;
	}

	KERN_PRT("kern finish %d", c);
}

extern "C" __global__ void wait_and_trg(int time)
{
	volatile int i = time;
	volatile int j;
	int c;

	KERN_PRT("start and wait %d", time);
	//cudaGridDependencySynchronize();
	KERN_PRT("run %d", time);
	for (j = 0; j < i; j++) {
		c = j;
	}

	KERN_PRT("trg next %d", time);
	cudaTriggerProgrammaticLaunchCompletion();

	for (j = 0; j < i; j++) {
		c = j;
	}
	KERN_PRT("finish %d", c);
}

extern "C" __global__ void only_wait(int time)
{
	volatile int i = time;
	volatile int j;
	int c;

	KERN_PRT("start and wait %d", time);
	cudaGridDependencySynchronize();
	KERN_PRT("run %d", time);
	for (j = 0; j < i; j++) {
		c = j;
	}

	KERN_PRT("finish %d", c);
}

extern "C" __global__ void event_loop(int time)
{
	volatile int i = time;
	volatile int j;
	int c;

	KERN_PRT("start %d", time);
	for (j = 0; j < i; j++) {
		c = j;
	}
	KERN_PRT("finish %d", c);
}




