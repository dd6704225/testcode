#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include "math.h"
#include "cuda.h"

#define CHECK(func) \
{   \
	cudaError_t ret; \
	if ((ret = func) != cudaSuccess) { \
		printf("Function Call %s retval %d Failed!\n", #func, ret);	\
		exit(1);	\
	}   \
}


#if 1
#define KERN_PRT(string, arg...) \
	printf("[KERN][%s][%d]: [" string "]\n", __func__, __LINE__, ##arg);
#else
#define KERN_PRT(string, arg...) {}
#endif

__global__ void basic_child(int *data)
{
	KERN_PRT("child[%d]", threadIdx.x);
	basic_child<<<1, 1, 0, cudaStreamTailLaunch>>>(data);
}

__global__ void basic_finish(int *data)
{
	KERN_PRT("finish[%d]", threadIdx.x);
}

__global__ void basic_start(int *data)
{
	KERN_PRT("basic[%d]", threadIdx.x);
	basic_child<<<1, 1>>>(data);

	basic_child<<<1, 1, 0, cudaStreamTailLaunch>>>(data);
}

static void basic(void)
{
	cudaStream_t stream;
	void *dev_a;
	size_t s = 1 * 1024 * 1024;

	CHECK(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));
	CHECK(cudaMalloc(&dev_a, s));

	CHECK(cudaMemsetAsync(dev_a, 0, s, stream));
	basic_start<<<1, 12, 0, stream>>>(dev_a);

	CHECK(cudaStreamSynchronize(stream));

	CHECK(cudaFree(dev_a));
	CHECK(cudaStreamDestroy(stream));
}


__global__ void inside_child(int *data)
{
	KERN_PRT("child[%d]", threadIdx.x);
}

__global__ void inside_finish(int *data)
{
	KERN_PRT("finish[%d]", threadIdx.x);
}

__global__ void inside(int *data)
{
	cudaStream_t stream;

	CHECK(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));

	ERR_MSG("start");
	inside_finish<<<1, 1, 0, cudaStreamTailLaunch>>>(data);
	inside_child<<<1, 1, 0, stream>>>(data);
}

static void stream_inside(void)
{
	cudaStream_t stream;
	void *dev_a;
	size_t s = 1 * 1024 * 1024;

	CHECK(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));
	CHECK(cudaMalloc(&dev_a, s));

	CHECK(cudaMemsetAsync(dev_a, 0, s, stream));
	inside<<<1, 12, 0, stream>>>(dev_a);

	CHECK(cudaStreamSynchronize(stream));

	CHECK(cudaFree(dev_a));
	CHECK(cudaStreamDestroy(stream));
}

int main(int argc, char **argv)
{
	printf("- Init...\n");
	CHECK(cudaSetDevice(0));

	basic();
	stream_inside();

	return 0;
}
