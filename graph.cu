#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include "math.h"
#include "cuda.h"

#define CHECK(func) \
{   \
	int ret; \
	if ((ret = func) != CUDA_SUCCESS) { \
		printf("Function Call %s retval %d Failed!\n", #func, ret);	\
		exit(1);	\
	}   \
}

/********************************/
CUdevice device;
CUcontext context;
CUmodule module;
CUfunction empty, mem_access, mem_accessExPre, mem_accessExPost;
CUfunction mem_accessPrt, ImmPrt, mem_accessWait, mem_accessGet;
CUfunction event_mid, event_loop, waitandTrg, onlywait;
void initCUDA(void)
{
	int devCount = 0;
	int value;

	CHECK(cuInit(0));
	CHECK(cuDeviceGetCount(&devCount));

	if (devCount == 0) {
		printf("No Device!\n");
		exit(1);
	}

	CHECK(cuDeviceGet(&device, 0));
	CHECK(cuCtxCreate(&context, 0, device));

	CHECK(cuDeviceGetAttribute(&value, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device));
	printf("attr compute cap major: %d\n", value);
	CHECK(cuDeviceGetAttribute(&value, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device));
	printf("attr compute cap minor: %d\n", value);

	CHECK(cuModuleLoad(&module, "test.cubin"));
	CHECK(cuModuleGetFunction(&empty, module, "empty"));
	CHECK(cuModuleGetFunction(&mem_access, module, "mem_access"));
	CHECK(cuModuleGetFunction(&mem_accessExPre, module, "mem_accessExPre"));
	CHECK(cuModuleGetFunction(&mem_accessExPost, module, "mem_accessExPost"));
	CHECK(cuModuleGetFunction(&mem_accessExPost, module, "mem_accessExPost"));
	CHECK(cuModuleGetFunction(&mem_accessPrt, module, "mem_accessPrt"));
	CHECK(cuModuleGetFunction(&ImmPrt, module, "ImmPrt"));
	CHECK(cuModuleGetFunction(&mem_accessWait, module, "mem_accessWait"));
	CHECK(cuModuleGetFunction(&mem_accessGet, module, "mem_accessGet"));
	CHECK(cuModuleGetFunction(&event_mid, module, "event_trgAtMid"));
	CHECK(cuModuleGetFunction(&event_loop, module, "event_loop"));
	CHECK(cuModuleGetFunction(&waitandTrg, module, "wait_and_trg"));
	CHECK(cuModuleGetFunction(&onlywait, module, "only_wait"));

}

void exitCUDA(void)
{
	CHECK(cuModuleUnload(module));
	CHECK(cuCtxDestroy(context));
}

static void basic(void)
{
	CUstream stream;
	CUgraph graph;
	CUgraphExec graph_exec;
	CUgraphNode nodes[20];
	size_t node_nums = 20;
	int loop = 1999999999;
	void *params[10] = {(void *)&loop};

	printf("%s start\n", __func__);
	CHECK(cuCtxSetCurrent(context));
	CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));

	CHECK(cuStreamBeginCapture(stream, CU_STREAM_CAPTURE_MODE_GLOBAL));

        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
	CHECK(cuStreamEndCapture(stream, &graph));

	CHECK(cuGraphGetNodes(graph, nodes, &node_nums));
	printf("nodes %lu\n", node_nums);
	for (size_t i = 0; i < node_nums-1; i++) {
		CUgraphEdgeData edges;
		size_t edge_nums = 1;
		memset(&edges, 0, sizeof(CUgraphEdgeData));
		CHECK(cuGraphGetEdges_v2(graph, &nodes[i], &nodes[i+1], &edges, &edge_nums));
		printf("n[%lu] to n[%lu] edges %lu\n", i, i+1, edge_nums);
		printf("                     %u %u\n", edges.from_port, edges.type);
	} 
	CHECK(cuGraphInstantiate(&graph_exec, graph, 0));

	//CHECK(cuGraphLaunch(graph_exec, stream));

	CHECK(cuStreamSynchronize(stream));
	printf("stream sync finish\n");

	CHECK(cuStreamDestroy(stream));
}

static void between_queue(void)
{
	CUstream stream1, stream2;
	CUevent event;
	CUlaunchConfig cfg = {0};
	CUlaunchConfig excfg = {0};
	CUlaunchAttribute attr[10];
	int loop = 999999999;
	void *params[10] = {(void *)&loop};

	printf("%s start\n", __func__);
	CHECK(cuCtxSetCurrent(context));
	CHECK(cuStreamCreate(&stream1, CU_STREAM_NON_BLOCKING));
	CHECK(cuStreamCreate(&stream2, CU_STREAM_NON_BLOCKING));
	CHECK(cuEventCreate(&event, CU_EVENT_DISABLE_TIMING));
	cfg.gridDimX = 1;
	cfg.gridDimY = 1;
	cfg.gridDimZ = 1;
	cfg.blockDimX = 1;
	cfg.blockDimY = 1;
	cfg.blockDimZ = 1;
	cfg.hStream = stream1;
	cfg.attrs = &attr[0];
	cfg.numAttrs = 1;
	attr[0].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_EVENT;
	attr[0].value.programmaticEvent.event = event;
	attr[0].value.programmaticEvent.flags = 0;
	attr[0].value.programmaticEvent.triggerAtBlockStart = 1;

	excfg.gridDimX = 1;
	excfg.gridDimY = 1;
	excfg.gridDimZ = 1;
	excfg.blockDimX = 1;
	excfg.blockDimY = 1;
	excfg.blockDimZ = 1;
	excfg.hStream = stream2;
	excfg.attrs = &attr[1];
	excfg.numAttrs = 1;
	attr[1].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION;
	attr[1].value.programmaticStreamSerializationAllowed = 1;

	CHECK(cuLaunchKernelEx(&cfg, event_mid, params, NULL));

	CHECK(cuStreamWaitEvent(stream2, event, 0));
	loop = 100;
	CHECK(cuLaunchKernelEx(&excfg, event_loop, params, NULL));

	CHECK(cuStreamSynchronize(stream2));
	printf("stream2 sync finish\n");

	CHECK(cuStreamSynchronize(stream1));
	printf("stream1 sync finish\n");

	CHECK(cuStreamDestroy(stream1));
	CHECK(cuStreamDestroy(stream2));
	CHECK(cuEventDestroy(event));
}

static void multi_trigger(void)
{
	CUstream stream;
	CUlaunchConfig cfg = {0};
	CUlaunchAttribute attr[10];
	int loop = 999999999;
	void *params[10] = {(void *)&loop};
	CUgraph graph;
	CUgraphExec graph_exec;
	CUgraphNode nodes[20];
	size_t node_nums = 20;

	CHECK(cuCtxSetCurrent(context));
	CHECK(cuStreamCreate(&stream, CU_STREAM_NON_BLOCKING));
	CHECK(cuStreamBeginCapture(stream, CU_STREAM_CAPTURE_MODE_GLOBAL));

	//loop = 99999;
        //CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));

	loop = 999999999;
	cfg.gridDimX = 1;
	cfg.gridDimY = 1;
	cfg.gridDimZ = 1;
	cfg.blockDimX = 1;
	cfg.blockDimY = 1;
	cfg.blockDimZ = 1;
	cfg.hStream = stream;
	cfg.attrs = attr;
	cfg.numAttrs = 1;
	attr[0].id = CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION;
	attr[0].value.programmaticStreamSerializationAllowed = 1;

	loop = 999999999;
	//first launch normal kernel with trigger and long loop
        CHECK(cuLaunchKernel(event_mid, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));

	loop = 99999;
	//second launch kernel which wait prev and trg a new completion
	CHECK(cuLaunchKernelEx(&cfg, waitandTrg, params, NULL));

	CHECK(cuLaunchKernelEx(&cfg, waitandTrg, params, NULL));

	loop = 100;
	//third launch another kernel only wait trigger
	CHECK(cuLaunchKernelEx(&cfg, onlywait, params, NULL));

        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));
	cfg.numAttrs = 0;
	CHECK(cuLaunchKernelEx(&cfg, event_loop, params, NULL));
        CHECK(cuLaunchKernel(event_loop, 1, 1, 1, 1, 1, 1, 0, stream, params, NULL));

	CHECK(cuStreamEndCapture(stream, &graph));
	CHECK(cuGraphDebugDotPrint(graph, "a.dot", 0xff));
	CHECK(cuGraphGetNodes(graph, nodes, &node_nums));
	printf("nodes %lu\n", node_nums);
	for (size_t i = 0; i < node_nums-1; i++) {
		CUgraphEdgeData edges;
		size_t edge_nums = 1;
		memset(&edges, 0, sizeof(CUgraphEdgeData));
		CHECK(cuGraphGetEdges_v2(graph, &nodes[i], &nodes[i+1], &edges, &edge_nums));
		printf("n[%lu] to n[%lu] edges %lu\n", i, i+1, edge_nums);
		printf("                     %u %u\n", edges.from_port, edges.type);
	} 
	CHECK(cuGraphInstantiate(&graph_exec, graph, 0));

	CHECK(cuGraphLaunch(graph_exec, stream));

	CHECK(cuStreamSynchronize(stream));
	printf("sync finish\n");
}



int main(int argc, char **argv)
{
	printf("- Init...\n");
	initCUDA();

	printf("start...\n");

	//basic();
	//between_queue();
	multi_trigger();

	exitCUDA();
	return 0;
}

