#!/bin/bash

#export PATH=/usr/local/cuda/bin:$PATH
#sudo cpufreq-set -g performance

nvcc test.cu -gencode arch=compute_90,code=sm_90 -cubin -O0
nvcc event.cu -o event -lcuda
nvcc graph.cu -o graph -lcuda
